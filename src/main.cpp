#include <Arduino.h>
#include <WiFiS3.h>

#include "Credentials.h"
#include "PumpCtl.h"
#include "QueueCtl.h"
#include "RelaisCtl.h"
#include "WaterLevelCtl.h"
#include "WaterLevelCtlConstants.h"
#include "WifiCtl.h"

// base values 
#define NAME "tanka"
#define PUMPONE_DIR "a2b"
#define PUMPONE_LPM 27.27
// #define PUMPTWO_DIR "b2a"
// #define PUMPTWO_LPM 27.27

// pins
#define READ_RESOLUTION 14
#define PUMPONE_RELAIS_PIN D2  // D2
// #define PUMPTWO_RELAIS_PIN D3 // D3
#define RESET_RELAIS_PIN D4  // D4
#define WL_SENSOR_PIN A2

// network
#define HOSTNAME "arduino-" NAME
#define BROKER "192.168.53.123"
#define BOKER_PORT 1883

// topics
#define ALIVETOPIC_PREFIX "home/keepalive/arduino/"
#define ALIVETOPIC ALIVETOPIC_PREFIX NAME
#define ALIVE_INTERVAL 3000

#define CTLTOPIC_PREFIX "home/control/"

#define RESET_CTLTOPIC CTLTOPIC_PREFIX NAME "/reset"

#define PUMPONE_CTLTOPIC CTLTOPIC_PREFIX NAME "/pump/" PUMPONE_DIR "/set"
#define PUMPONE_ACTTOPIC CTLTOPIC_PREFIX NAME "/pump/" PUMPONE_DIR "/actual"
// #define PUMPTWO_CTLTOPIC CTLTOPIC_PREFIX NAME "/pump/" PUMPTWO_DIR "/set"
// #define PUMPTWO_ACTTOPIC CTLTOPIC_PREFIX NAME "/pump/" PUMPTWO_DIR "/actual"

#define WATERLEVEL_TOPIC_PREFIX "home/sensor/waterlevel/"
#define WATERLEVEL_TOPIC WATERLEVEL_TOPIC_PREFIX NAME

#define SHUNT_TOPIC_PREFIX "home/sensor/power/"
#define SHUNTBAT_TOPIC SHUNT_TOPIC_PREFIX NAME "/battery"

// messure water level
#define WL_SENSOR_MIN 1545
#define WL_SENSOR_MAX 2340
#define WL_LITER_MIN 0
#define WL_LITER_MAX 300

// create object for wifi, wificlient and mqttclient
CWifi wifi;
WiFiClient wifiClient;
MqttClient mqttClient(wifiClient);

// create our controller objects for wifi/mqtt/relais/waterlevel
WifiCtl wifiCtl(WIFI_SSID, WIFI_PASS, HOSTNAME, wifi, wifiClient);
QueueCtl queueCtl(mqttClient, BROKER, BOKER_PORT, ALIVETOPIC, ALIVE_INTERVAL,
                  WATERLEVEL_TOPIC, SHUNTBAT_TOPIC);
RelaisCtl pumpOneRelais(PUMPONE_RELAIS_PIN, LOW);
// RelaisCtl pumpTwoRelais(PUMPTWO_RELAIS_PIN, LOW);
RelaisCtl resetRelais(RESET_RELAIS_PIN, LOW);
WaterLevelCtl waterLevel(WL_SENSOR_PIN, WL_SENSOR_MIN, WL_SENSOR_MAX,
                         WL_LITER_MIN, WL_LITER_MAX, WL_MEASURE_INTERVAL,
                         WL_MEASURE_MAX, WL_FLATEN_OFFSET);
PumpCtl pumpOne(pumpOneRelais, PUMPONE_DIR, PUMPONE_LPM);
// PumpCtl pumpTwo(pumpTwoRelais, PUMPTWO_DIR, PUMPTWO_LPM);

void resetHandler() {
  Serial.println("resetHandler - triggered.. bye!!");
  pumpOneRelais.off();
  // pumpTwoRelais.off();

  queueCtl.disconnect();
  wifiCtl.disconnect();
  Serial.end();

  // and we use the reset relais to restart ourself
  resetRelais.on();
  while (true) {
    // we should never land here btw..
    // but this keeps ardiono from looping in reboot case
  }
}

void messageHandler(int messageSize) {
  if (queueCtl._client.messageDup()) {
    Serial.println("messageHandler - duplicated msg received - skipping");
    return;
  }

  String messageTopic = queueCtl._client.messageTopic();
  String message = "";

  Serial.print("messageHandler - received msg on topic: ");
  Serial.println(messageTopic);

  for (int i = 0; i < messageSize; i++) {
    // we read each byte from the message
    char byteRead = (char)queueCtl._client.read();
    // check if the byte is a digit
    if (isdigit(byteRead)) {
      message += byteRead;
    } else {
      // if not we return out of the handler
      Serial.println("messageHandler - received jibberish, returning");
      return;
    }
  }

  // we kinda also read every byte in case we get shit
  // cause if we get a value for exmplae like "23b10"
  // we actualy do nothing cause we returned up there
  // so we do later only on correct integer value message

  if (messageTopic == RESET_CTLTOPIC) {
    // for now we have no logic like reset in a certain time
    // like we do later with the pumpRuntime
    // but we can do that later, for now reset will be triggered if correct
    // topic and message is an integer value
    resetHandler();
  } else if (messageTopic == PUMPONE_CTLTOPIC) {
    int liter = message.toInt();
    pumpOne.setRuntimeLiter(liter);
    // } else if (messageTopic == PUMPTWO_CTLTOPIC) {
    // int liter = message.toInt();
    // pumpTwo.setRuntimeLiter(liter);
  } else {
    return;
  }
}

void setup() {
  // init serial
  Serial.begin(9600);
  while (!Serial) {
    ;  // wait for serial port to connect. Needed for native USB port only
  }
  delay(5000);

  Serial.println();  // new line at the start if the program
  Serial.println(
      "setup is running, give it a little time to clean everything up and "
      "reconnect...");

  // set analog read resolution
  analogReadResolution(READ_RESOLUTION);

  // init relais
  pumpOneRelais.init();
  // pumpTwoRelais.init();
  resetRelais.init();

  // init wifi and mqtt
  wifiCtl.connect(resetHandler);
  queueCtl.connect(resetHandler);
  queueCtl._client.onMessage(messageHandler);
  queueCtl._client.subscribe(RESET_CTLTOPIC);
  queueCtl._client.subscribe(PUMPONE_CTLTOPIC);
  // queueCtl._client.subscribe(PUMPTWO_CTLTOPIC);

  // init water level measurement
  waterLevel.init();
}

void loop() {
  // delay(1500);

  pumpOne.process();
  // pumpTwo.process();

  // then we do checks and alive
  if (!wifi.status() || !queueCtl.status()) {
    resetHandler();
  }
  queueCtl.sendAlive();

  // then we do measure and sending results if ready
  waterLevel.measure();

  if (waterLevel.getState() == 3) {
    // in this case we can send the result to the queue
    int* results = waterLevel.getResults();
    queueCtl.sendWLMeasures(results);
  }

  // do a dirty every day reset -> 1 * 24 * 60 * 60 * 1000
  // for testing do every 20min -> 20 * 60 * 1000
  if (millis() > 1 * 24 * 60 * 60 * 1000) {
    resetHandler();
  }
}
